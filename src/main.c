#include <stdint.h>
#include <stdio.h>
#include "../include/arraylib.h"

#define ARRAY_SIZE 10

int main(int argc, char *argv[]) {
    int16_t array[ARRAY_SIZE] = { 
        0x1124, 0x0012, 0x0235, 0x1212, 0xF458, 
        0x0068, 0x4545, 0x4352, 0x1223, 0x4444 
    };

    printf("Transformación de números de un array \n");
    printf("Array de entrada: \n {");
    for(uint32_t index = 0; index < ARRAY_SIZE - 1; index++)
        printf("0x%04x, ", array[index]);
    printf("0x%04x}\n", array[ARRAY_SIZE - 1]);
    
    printf("Valores de entrada: \n {");
    for(uint32_t index = 0; index < ARRAY_SIZE - 1; index++)
        printf("%d, ", array[index]);
    printf("%d}\n", array[ARRAY_SIZE - 1]);
    
    process_array(array, ARRAY_SIZE);
    
    printf("Array de salida: \n {");
    for(uint32_t index = 0; index < ARRAY_SIZE - 1; index++)
        printf("0x%04x, ", array[index]);
    printf("0x%04x}\n", array[ARRAY_SIZE - 1]);

    printf("Valores de salida: \n {");
    for(uint32_t index = 0; index < ARRAY_SIZE - 1; index++)
        printf("%d, ", array[index]);
    printf("%d}\n", array[ARRAY_SIZE - 1]);
    return 0;
}