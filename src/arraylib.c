#include "../include/arraylib.h"

void execute_three_strategy(int16_t* array, const uint32_t index) {
    array[index] = (array[index] * DEFAULT_VALUE) & 0b1111111111111100;
}

void execute_two_strategy(int16_t* array, const uint32_t index) {
    array[index] = ~array[index];
}

void execute_one_strategy(int16_t* array, const uint32_t index) {
    uint8_t count = index < 5 ? 0 : index - 5;
    int32_t result = 0;

    for(; count < index; count++) {
        result += ((int32_t) array[count]);
    }
    array[index] = result / 5;
}

void execute_zero_strategy(int16_t* array, const uint32_t index) {
    array[index] = array[index] >> 1;
}

void process_value(int16_t* array, const uint32_t index) {
    int8_t end_bits = array[index] & 0b00000011;

    switch (end_bits)
    {
    case 0:
        execute_zero_strategy(array, index);
        break;
    case 1:
        execute_one_strategy(array, index);
        break;
    case 2:
        execute_two_strategy(array, index);
        break;
    case 3:
        execute_three_strategy(array, index);
        break;
    }
}

bool is_end_of_array(int16_t* array, const uint32_t size, const uint32_t index) {
    if(index >= size)
        return true;

    return array[index] == ARRAY_END_CODE;
}

void process_array(int16_t* array, const uint32_t size) {
    uint32_t index = 0;

    while (!is_end_of_array(array, size, index))
    {
        process_value(array, index);
        index++;
    }
}