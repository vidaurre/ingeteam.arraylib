#ifndef __ARRAYLIB_H__
#define __ARRAYLIB_H__

#include <stdint.h>
#include <stdbool.h>

#define ARRAY_END_CODE 0x4545
#define DEFAULT_VALUE  0x0006

void process_array(int16_t* array, const uint32_t size);

#endif